import React from 'react';
import Application from './src/app';

const App = () => (
    <Application />
);

export default App;
