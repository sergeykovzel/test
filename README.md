# README #

REACT NATIVE APPLICATION

# TECHNOLOGIES #

- React Native
- Redux
- Redux-saga

# LAUNCH GUIDE #

To launch the application you have to:

1. Install React Native and all that is required (check guide on react native web-site)
```https://facebook.github.io/react-native/```
2. Clone this repo
3. Open project folder and run on your console
```npm i```
4. Run on your console
```react-native run-android```