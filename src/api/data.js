import request from './../helpers/HttpHelper';

const loadData = () => request.get('http://www.mocky.io/v2/59f08692310000b4130e9f71');

export {
    loadData,
};
