import React from 'react';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import reducers from './redux/reducers';
import RootRouter from './routers';
import rootSaga from './sagas';

const sagaMiddleware = createSagaMiddleware();
const createStoreWithMiddleware = applyMiddleware(sagaMiddleware)(createStore);
const store = createStoreWithMiddleware(reducers);

sagaMiddleware.run(rootSaga);

const Application = () => (
    <Provider store={store}>
        <RootRouter onNavigationStateChange={null} />
    </Provider>
);

export default Application;
