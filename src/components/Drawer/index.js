import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';

import IconButton from './../buttons/IconButton';
import DrawerEvent from './../../containers/DrawerEvent';
import ScrollList from './../ScrollList';

import * as icons from './../../constants/icons';
import * as colors from './../../constants/colors';

import styles from './styles';

class Drawer extends Component {
    static propTypes = {
        events: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.string,
            name: PropTypes.arrayOf(PropTypes.string),
        })),
        navigation: PropTypes.shape({
            navigate: PropTypes.func,
        }),
    }

    static defaultProps = {
        events: [],
        navigation: {
            navigate: () => {},
        },
    }

    static renderEvent(event) {
        return (
            <DrawerEvent
                key={event.id}
                event={event}
            />
        );
    }

    constructor(props) {
        super(props);

        this.closeDrawer = this.closeDrawer.bind(this);
    }

    closeDrawer() {
        const { navigation } = this.props;
        navigation.navigate('DrawerClose');
    }

    render() {
        const { events } = this.props;
        return (
            <View style={styles.wrapper}>
                <View style={styles.actionRow}>
                    <IconButton
                        type={icons.ICON_CLOSE}
                        onPress={this.closeDrawer}
                        iconColor={colors.DEFAULT_COLOR}
                    />
                </View>
                <ScrollList>
                    {events.map(Drawer.renderEvent)}
                </ScrollList>
            </View>
        );
    }
}

export default Drawer;
