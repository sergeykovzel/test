import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
    },
    actionRow: {
        alignItems: 'flex-end',
    },
});

export default styles;
