import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';

import DrawerMarket from './../../containers/DrawerMarket';

class DrawerEvent extends PureComponent {
    static propTypes = {
        markets: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.string,
            name: PropTypes.string,
        })),
    }

    static defaultProps = {
        markets: [],
    }

    static renderMarket(market) {
        return (
            <DrawerMarket
                key={market.id}
                market={market}
            />
        );
    }

    render() {
        const { markets } = this.props;
        return (
            <View>
                {markets.map(DrawerEvent.renderMarket)}
            </View>
        );
    }
}

export default DrawerEvent;
