import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';

import DrawerSelection from './../../containers/DrawerSelection';

class DrawerMarket extends PureComponent {
    static propTypes = {
        market: PropTypes.shape({
            id: PropTypes.string,
            name: PropTypes.string,
        }),
        selections: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.string,
            name: PropTypes.string,
            price: PropTypes.number,
        })),
        activeSelections: PropTypes.arrayOf(PropTypes.string),
    }

    static defaultProps = {
        market: PropTypes.shape({
            id: `MKT_${Math.random()}`,
            name: 'Unknown market',
        }),
        selections: [],
        activeSelections: [],
    }

    constructor(props) {
        super(props);

        this.renderSelection = this.renderSelection.bind(this);
    }

    renderSelection(selection) {
        const { market, activeSelections } = this.props;
        if (activeSelections.includes(selection.id)) {
            return (
                <DrawerSelection
                    key={selection.id}
                    selection={selection}
                    type={market.name}
                />
            );
        }
        return null;
    }

    render() {
        const { selections } = this.props;
        return (
            <View>
                {selections.map(this.renderSelection)}
            </View>
        );
    }
}

export default DrawerMarket;
