import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    Text,
    View,
} from 'react-native';

import TextButton from './../buttons/TextButton';

import styles from './styles';

class DrawerSelection extends Component {
    static propTypes = {
        selection: PropTypes.shape({
            id: PropTypes.string,
            name: PropTypes.string,
            price: PropTypes.number,
        }),
        unselect: PropTypes.func,
        type: PropTypes.string,
    }

    static defaultProps = {
        selection: PropTypes.shape({
            id: `SEL_${Math.random()}`,
            name: 'Unknown selection',
            price: 0,
        }),
        unselect: () => {},
        type: '-',
    }

    constructor(props) {
        super(props);

        this.unselect = this.unselect.bind(this);
    }

    unselect() {
        const {
            selection,
            unselect,
        } = this.props;

        unselect(selection.id);
    }

    render() {
        const { selection, type } = this.props;

        return (
            <View style={styles.wrapper}>
                <Text
                    style={styles.text}
                    numberOfLines={1}
                >
                    {`${selection.name} ${type}`}
                </Text>
                <Text
                    style={styles.text}
                    numberOfLines={1}
                >
                    {selection.price}
                </Text>
                <TextButton
                    key={selection.id}
                    label={'Delete'}
                    onPress={this.unselect}
                    style={styles.button}
                />
            </View>
        );
    }
}

export default DrawerSelection;
