import { StyleSheet } from 'react-native';

import * as colors from './../../constants/colors';

const styles = StyleSheet.create({
    wrapper: {
        borderColor: colors.BORDER_COLOR,
        borderWidth: 1,
        padding: 5,
        marginVertical: 5,
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        borderWidth: 1,
        marginHorizontal: 5,
        marginVertical: 5,
        paddingVertical: 5,
        backgroundColor: colors.NEUTRAL_COLOR,
        borderColor: colors.BORDER_COLOR,
    },
    text: {
        textAlign: 'center',
        marginVertical: 5,
    },
});

export default styles;
