import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
    Text,
    View,
} from 'react-native';

import Market from './../../containers/Market';

import styles from './styles';

class Event extends PureComponent {
    static propTypes = {
        event: PropTypes.shape({
            id: PropTypes.string,
            name: PropTypes.arrayOf(PropTypes.string),
        }),
        markets: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.string,
            name: PropTypes.string,
        })),
    }

    static defaultProps = {
        event: PropTypes.shape({
            id: `EVT_${Math.random()}`,
            name: ['', ''],
        }),
        markets: [],
    }

    static renderMarket(market) {
        return (
            <Market
                key={market.id}
                market={market}
            />
        );
    }

    render() {
        const { event, markets } = this.props;
        return (
            <View style={styles.wrapper}>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={[styles.text, styles.name]} numberOfLines={1}>{event.name[0]}</Text>
                    <Text style={[styles.text, styles.separator]} numberOfLines={1}>{'vs'}</Text>
                    <Text style={[styles.text, styles.name]} numberOfLines={1}>{event.name[1]}</Text>
                </View>
                {markets.map(Event.renderMarket)}
            </View>
        );
    }
}

export default Event;
