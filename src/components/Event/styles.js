import { StyleSheet } from 'react-native';

import * as colors from './../../constants/colors';

const styles = StyleSheet.create({
    wrapper: {
        marginVertical: 5,
        backgroundColor: '#fff',
    },
    text: {
        fontSize: 15,
        textAlign: 'center',
        padding: 10,
        backgroundColor: colors.PRIMARY_COLOR,
        color: '#fff',
    },
    name: {
        flex: 2,
    },
    separator: {
        flex: 2,
    },
});

export default styles;
