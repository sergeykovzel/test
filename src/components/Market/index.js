import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
    Text,
    View,
} from 'react-native';

import SelectionsGroup from './../SelectionsGroup';
import Selection from './../../containers/Selection';

import styles from './styles';

class Market extends PureComponent {
    static propTypes = {
        market: PropTypes.shape({
            id: PropTypes.string,
            name: PropTypes.string,
        }),
        selections: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.string,
            name: PropTypes.string,
            price: PropTypes.number,
        })),
    }

    static defaultProps = {
        market: PropTypes.shape({
            id: `MKT_${Math.random()}`,
            name: 'Unknown market',
        }),
        selections: [],
    }

    static renderSelection(selection) {
        return (
            <Selection
                key={selection.id}
                selection={selection}
            />
        );
    }

    render() {
        const { market, selections } = this.props;
        return (
            <View style={styles.wrapper}>
                <Text style={styles.label}>{market.name}</Text>
                <SelectionsGroup>
                    {selections.map(Market.renderSelection)}
                </SelectionsGroup>
            </View>
        );
    }
}

export default Market;
