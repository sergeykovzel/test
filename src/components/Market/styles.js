import { StyleSheet } from 'react-native';

import * as colors from './../../constants/colors';

const styles = StyleSheet.create({
    wrapper: {
        marginBottom: 10,
    },
    label: {
        fontSize: 16,
        textAlign: 'center',
        padding: 10,
        color: colors.DEFAULT_COLOR,
    },
});

export default styles;
