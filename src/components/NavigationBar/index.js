import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import styles from './styles';

class NavigationBar extends PureComponent {
    static propTypes = {
        children: PropTypes.oneOfType([
            PropTypes.arrayOf(PropTypes.node),
            PropTypes.node,
        ]),
    }

    static defaultProps = {
        children: null,
    }
    render() {
        const { children } = this.props;
        return (
            <View style={styles.wrapper}>
                {children}
            </View>
        );
    }
}

export default NavigationBar;
