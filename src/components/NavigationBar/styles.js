import { StyleSheet } from 'react-native';

import * as colors from './../../constants/colors';

const styles = StyleSheet.create({
    wrapper: {
        width: '100%',
        height: 52,
        paddingHorizontal: 4,
        backgroundColor: colors.PRIMARY_COLOR,
        flexDirection: 'row',
        alignItems: 'center',
    },
});

export default styles;
