import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
    Text,
    View,
} from 'react-native';

import styles from './styles';

class NavigationTitle extends PureComponent {
    static propTypes = {
        value: PropTypes.string,
    }

    static defaultProps = {
        value: '',
    }
    render() {
        const { value } = this.props;
        return (
            <View style={styles.wrapper}>
                <Text style={styles.label} numberOfLines={1}>{value}</Text>
            </View>
        );
    }
}

export default NavigationTitle;
