import { StyleSheet } from 'react-native';

import * as colors from './../../constants/colors';

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    label: {
        fontSize: 18,
        backgroundColor: 'transparent',
        color: colors.TEXT_LIGHT_COLOR,
        textAlign: 'left',
        paddingHorizontal: 18,
    },
});

export default styles;
