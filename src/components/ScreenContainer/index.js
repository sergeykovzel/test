import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';

import NavigationBar from './../NavigationBar';
import NavigationTitle from './../NavigationTitle';
import IconButton from './../buttons/IconButton';

import * as icons from './../../constants/icons';
import * as colors from './../../constants/colors';

import styles from './styles';

class ScreenContainer extends PureComponent {
    static propTypes = {
        children: PropTypes.oneOfType([
            PropTypes.arrayOf(PropTypes.node),
            PropTypes.node,
        ]),
        screenTitle: PropTypes.string,
        navigation: PropTypes.shape({
            navigate: PropTypes.func,
        }),
    }

    static defaultProps = {
        children: null,
        screenTitle: '',
        navigation: {
            navigate: () => {},
        },
    }

    constructor(props) {
        super(props);

        this.openDrawer = this.openDrawer.bind(this);
    }

    openDrawer() {
        const { navigation } = this.props;
        navigation.navigate('DrawerOpen');
    }

    render() {
        const { children, screenTitle } = this.props;

        return (
            <View style={styles.wrapper}>
                <NavigationBar>
                    <NavigationTitle value={screenTitle} />
                    <IconButton
                        type={icons.ICON_MENU}
                        onPress={this.openDrawer}
                        iconColor={colors.TEXT_LIGHT_COLOR}
                    />
                </NavigationBar>
                {children}
            </View>
        );
    }
}

export default ScreenContainer;
