import { StyleSheet } from 'react-native';

import * as colors from './../../constants/colors';

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: colors.MAIN_BACKGROUND_COLOR,
    },
});

export default styles;
