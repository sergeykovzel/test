import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { ScrollView } from 'react-native';

import styles from './styles';

class ScrollList extends PureComponent {
    static propTypes = {
        children: PropTypes.oneOfType([
            PropTypes.arrayOf(PropTypes.node),
            PropTypes.node,
        ]),
    }

    static defaultProps = {
        children: null,
    }

    render() {
        const { children } = this.props;
        return (
            <ScrollView contentContainerStyle={styles.wrapper}>
                {children}
            </ScrollView>
        );
    }
}

export default ScrollList;
