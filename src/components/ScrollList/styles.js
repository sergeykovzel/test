import { StyleSheet } from 'react-native';

import * as colors from './../../constants/colors';

const styles = StyleSheet.create({
    wrapper: {
        paddingVertical: 5,
        paddingHorizontal: 10,
    },
});

export default styles;
