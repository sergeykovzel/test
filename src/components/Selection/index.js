import React, { Component } from 'react';
import PropTypes from 'prop-types';

import TextButton from './../buttons/TextButton';

import styles from './styles';

class Selection extends Component {
    static propTypes = {
        selection: PropTypes.shape({
            id: PropTypes.string,
            name: PropTypes.string,
            price: PropTypes.number,
        }),
        isActive: PropTypes.bool,
        select: PropTypes.func,
        unselect: PropTypes.func,
    }

    static defaultProps = {
        selection: PropTypes.shape({
            id: `SEL_${Math.random()}`,
            name: 'Unknown selection',
            price: 0,
        }),
        isActive: false,
        select: () => {},
        unselect: () => {},
    }

    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        const {
            selection,
            isActive,
            select,
            unselect,
        } = this.props;

        isActive ? unselect(selection.id) : select(selection.id);
    }

    render() {
        const { selection, isActive } = this.props;

        return (
            <TextButton
                key={selection.id}
                label={`${selection.name}\n${selection.price}`}
                onPress={this.toggle}
                style={[styles.button, isActive ? styles.activeButton : styles.inactiveButton]}
            />
        );
    }
}

export default Selection;
