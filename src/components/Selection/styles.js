import { StyleSheet } from 'react-native';

import * as colors from './../../constants/colors';

const styles = StyleSheet.create({
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        borderWidth: 1,
        marginHorizontal: 5,
        paddingVertical: 5,
    },
    activeButton: {
        borderColor: colors.DARK_POSITIVE_COLOR,
        backgroundColor: colors.LIGHT_POSITIVE_COLOR,
    },
    inactiveButton: {
        borderColor: colors.BORDER_COLOR,
        backgroundColor: 'transparent',
    },
});

export default styles;
