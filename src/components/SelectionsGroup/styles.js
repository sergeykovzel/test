import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    wrapper: {
        flexDirection: 'row',
        marginHorizontal: 5,
    },
});

export default styles;
