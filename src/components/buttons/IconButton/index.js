import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    TouchableOpacity,
    Image,
} from 'react-native';

import * as colors from './../../../constants/colors';
import styles from './styles';

class IconButton extends Component {
    static propTypes = {
        type: PropTypes.number,
        iconColor: PropTypes.string,
    }

    static defaultProps = {
        type: 0,
        iconColor: colors.DEFAULT_COLOR,
    }

    render() {
        const { type, iconColor } = this.props;
        return (
            <TouchableOpacity
                style={styles.wrapper}
                {...this.props}
            >
                <Image
                    source={type}
                    style={[styles.icon, iconColor ? { tintColor: iconColor } : null]}
                />
            </TouchableOpacity>
        );
    }
}

export default IconButton;
