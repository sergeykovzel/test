import {
    StyleSheet,
} from 'react-native';

import * as colors from './../../../constants/colors';

const styles = StyleSheet.create({
    wrapper: {
        width: 46,
        height: 52,
        justifyContent: 'center',
        alignItems: 'center',
    },
    icon: {
        tintColor: colors.TEXT_LIGHT_COLOR,
        resizeMode: 'contain',
        height: 22,
        width: 22,
    },
});

export default styles;
