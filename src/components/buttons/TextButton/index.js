import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    TouchableOpacity,
    Text,
} from 'react-native';

import styles from './styles';

class TextButton extends Component {
    static propTypes = {
        label: PropTypes.string,
    }

    static defaultProps = {
        label: '-',
    }

    render() {
        const { label } = this.props;
        return (
            <TouchableOpacity
                style={styles.wrapper}
                {...this.props}
            >
                <Text style={styles.label}>{label}</Text>
            </TouchableOpacity>
        );
    }
}

export default TextButton;
