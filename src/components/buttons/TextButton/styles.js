import { StyleSheet } from 'react-native';

import * as colors from './../../../constants/colors';

const styles = StyleSheet.create({
    wrapper: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        borderWidth: 1,
        borderColor: colors.BORDER_COLOR,
        marginHorizontal: 5,
        paddingVertical: 5,
    },
    label: {
        textAlign: 'center',
        fontSize: 14,
    },
});

export default styles;
