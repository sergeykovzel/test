import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import ScreenContainer from './../../ScreenContainer';
import ScrollList from './../../ScrollList';
import Event from './../../../containers/Event';

class EventsScreen extends PureComponent {
    static propTypes = {
        events: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.string,
            name: PropTypes.arrayOf(PropTypes.string),
        })),
        loadData: PropTypes.func,
        navigation: PropTypes.shape({
            navigate: PropTypes.func,
        }),
    }

    static defaultProps = {
        events: [],
        loadData: () => {},
        navigation: {
            navigate: () => {},
        },
    }

    static renderEvent(event) {
        return (
            <Event
                key={event.id}
                event={event}
            />
        );
    }

    componentWillMount() {
        const { loadData } = this.props;
        loadData();
    }

    render() {
        const { events, navigation } = this.props;

        return (
            <ScreenContainer
                navigation={navigation}
                screenTitle={'Events'}
            >
                <ScrollList>
                    {events.map(EventsScreen.renderEvent)}
                </ScrollList>
            </ScreenContainer>
        );
    }
}

export default EventsScreen;
