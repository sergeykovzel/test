const ICON_MENU = require('./../assets/icons/menu.png');
const ICON_CLOSE = require('./../assets/icons/close.png');

export {
    ICON_MENU,
    ICON_CLOSE,
};
