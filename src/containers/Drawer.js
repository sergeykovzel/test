import { connect } from 'react-redux';

import DrawerComponent from './../components/Drawer';

const mapStateToProps = (state, ownProps) => ({
    events: state.EventsReducer.events,
});

const mapDispatchToProps = (dispatch, ownProps) => ({});

const Drawer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(DrawerComponent);

export default Drawer;
