import { connect } from 'react-redux';

import DrawerEventComponent from './../components/DrawerEvent';

const mapStateToProps = (state, ownProps) => ({
    markets: state.MarketsReducer.markets[ownProps.event.id],
});

const mapDispatchToProps = (dispatch, ownProps) => ({});

const DrawerEvent = connect(
    mapStateToProps,
    mapDispatchToProps,
)(DrawerEventComponent);

export default DrawerEvent;
