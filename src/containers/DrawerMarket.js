import { connect } from 'react-redux';

import DrawerMarketComponent from './../components/DrawerMarket';

const mapStateToProps = (state, ownProps) => ({
    selections: state.SelectionsReducer.selections[ownProps.market.id],
    activeSelections: state.SelectionsReducer.activeSelections,
});

const mapDispatchToProps = (dispatch, ownProps) => ({});

const DrawerMarket = connect(
    mapStateToProps,
    mapDispatchToProps,
)(DrawerMarketComponent);

export default DrawerMarket;
