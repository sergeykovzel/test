import { connect } from 'react-redux';

import { unselect } from './../redux/actions/SelectionsActions';

import DrawerSelectionComponent from './../components/DrawerSelection';

const mapStateToProps = (state, ownProps) => ({});

const mapDispatchToProps = (dispatch, ownProps) => ({
    unselect: (id) => {
        dispatch(unselect(id));
    },
});

const DrawerSelection = connect(
    mapStateToProps,
    mapDispatchToProps,
)(DrawerSelectionComponent);

export default DrawerSelection;
