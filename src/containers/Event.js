import { connect } from 'react-redux';

import EventComponent from './../components/Event';

const mapStateToProps = (state, ownProps) => ({
    markets: state.MarketsReducer.markets[ownProps.event.id],
});

const mapDispatchToProps = (dispatch, ownProps) => ({});

const Event = connect(
    mapStateToProps,
    mapDispatchToProps,
)(EventComponent);

export default Event;
