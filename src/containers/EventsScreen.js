import { connect } from 'react-redux';

import {
    loadData,
} from './../redux/actions/DataActions';

import EventsScreenComponent from './../components/screens/EventsScreen';

const mapStateToProps = (state, ownProps) => ({
    events: state.EventsReducer.events,
});

const mapDispatchToProps = (dispatch, ownProps) => ({
    loadData: () => {
        dispatch(loadData());
    },
});

const EventsScreen = connect(
    mapStateToProps,
    mapDispatchToProps,
)(EventsScreenComponent);

export default EventsScreen;
