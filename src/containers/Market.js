import { connect } from 'react-redux';

import MarketComponent from './../components/Market';

const mapStateToProps = (state, ownProps) => ({
    selections: state.SelectionsReducer.selections[ownProps.market.id],
});

const mapDispatchToProps = (dispatch, ownProps) => ({});

const Market = connect(
    mapStateToProps,
    mapDispatchToProps,
)(MarketComponent);

export default Market;
