import { connect } from 'react-redux';

import { select, unselect } from './../redux/actions/SelectionsActions';

import SelectionComponent from './../components/Selection';

const mapStateToProps = (state, ownProps) => ({
    isActive: Boolean(state.SelectionsReducer.activeSelections.includes(ownProps.selection.id)),
});

const mapDispatchToProps = (dispatch, ownProps) => ({
    select: (id) => {
        console.log('id: ', id);
        dispatch(select(id));
    },
    unselect: (id) => {
        dispatch(unselect(id));
    },
});

const Selection = connect(
    mapStateToProps,
    mapDispatchToProps,
)(SelectionComponent);

export default Selection;
