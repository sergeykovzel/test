const organizeEvent = event => ({
    id: event.id,
    name: event.name.split('vs').map(item => item.trim()),
});

const organizeMarket = market => ({
    id: market.id,
    name: market.name.replace(/^((Team\s)|(Player\s))/, '').replace(/(Win)/, 'WIN'),
});

const organizeSelection = selection => ({
    id: selection.id,
    name: selection.name,
    price: selection.price,
});

const organizeData = (data) => {
    const result = {
        events: [],
        markets: {},
        selections: {},
    };

    data.forEach((event) => {
        if (event.markets.length > 0) {
            result.events.push(organizeEvent(event));
            result.markets[event.id] = [];

            event.markets.forEach((market) => {
                result.markets[event.id].push(organizeMarket(market));
                result.selections[market.id] = [];

                market.selections.forEach((selection) => {
                    result.selections[market.id].push(organizeSelection(selection));
                });
            });
        }
    });

    return result;
};

export default organizeData;
