const GET = 'GET';
const POST = 'POST';
const PUT = 'PUT';
const DELETE = 'DELETE';

const httpRequest = async (url, method, data = {}) => {
    try {
        const requestConfig = {
            method,
            headers: {
                'Content-Type': 'application/json',
            },
        };

        return fetch(url, requestConfig)
            .then(response => response.json())
            .then(responseJSON => responseJSON)
            .catch(error => console.log(error));
    } catch (exception) {
        throw exception;
    }
};

const request = {
    get: url => httpRequest(url, GET),
    post: (url, data) => httpRequest(url, POST, data),
    put: (url, data) => httpRequest(url, PUT, data),
    delete: (url, data) => httpRequest(url, DELETE, data),
};

export default request;
