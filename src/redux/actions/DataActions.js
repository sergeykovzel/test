import { createAction } from 'redux-actions';
import * as types from './../action-types/DataActionTypes';

const loadData = createAction(types.LOAD_DATA);

const loadDataFail = createAction(
    types.LOAD_DATA_FAILED,
    message => ({ message }),
);

const clearErrorMessage = createAction(types.CLEAR_ERROR_MESSAGE);

export {
    loadData,
    loadDataFail,
    clearErrorMessage,
};
