import { createAction } from 'redux-actions';
import * as types from './../action-types/EventsActionTypes';

const setData = createAction(
    types.EVENTS_SET_DATA,
    events => ({ events }),
);

export {
    setData,
};
