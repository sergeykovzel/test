import { createAction } from 'redux-actions';
import * as types from './../action-types/MarketsActionTypes';

const setData = createAction(
    types.MARKETS_SET_DATA,
    markets => ({ markets }),
);

export {
    setData,
};
