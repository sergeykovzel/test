import { createAction } from 'redux-actions';
import * as types from './../action-types/SelectionsActionTypes';

const setData = createAction(
    types.SELECTIONS_SET_DATA,
    selections => ({ selections }),
);

const select = createAction(
    types.SELECTIONS_SELECT,
    id => ({ id }),
);

const unselect = createAction(
    types.SELECTIONS_UNSELECT,
    id => ({ id }),
);

export {
    setData,
    select,
    unselect,
};
