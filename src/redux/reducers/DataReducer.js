import * as types from './../action-types/DataActionTypes';

const initialState = {
    errorMessage: '',
};

const DataReducer = (state = initialState, action) => {
    switch (action.type) {
    case types.LOAD_DATA_FAILED:
        return Object.assign({}, state, {
            message: action.message,
        });

    case types.CLEAR_ERROR_MESSAGE:
        return Object.assign({}, state, {
            errorMessage: '',
        });

    case types.LOAD_DATA:
    default:
        return state;
    }
};

export default DataReducer;
