import * as types from './../action-types/EventsActionTypes';

const initialState = {
    events: [],
};

const EventsReducer = (state = initialState, action) => {
    switch (action.type) {
    case types.EVENTS_SET_DATA:
        return Object.assign({}, state, {
            events: action.events,
        });

    default:
        return state;
    }
};

export default EventsReducer;
