import * as types from './../action-types/MarketsActionTypes';

const initialState = {
    markets: {},
};

const MarketsReducer = (state = initialState, action) => {
    switch (action.type) {
    case types.MARKETS_SET_DATA:
        return Object.assign({}, state, {
            markets: action.markets,
        });

    default:
        return state;
    }
};

export default MarketsReducer;
