import * as types from './../action-types/SelectionsActionTypes';

const initialState = {
    selections: {},
    activeSelections: [],
};

const SelectionsReducer = (state = initialState, action) => {
    switch (action.type) {
    case types.SELECTIONS_SET_DATA:
        return Object.assign({}, state, {
            selections: action.selections,
        });

    case types.SELECTIONS_SELECT:
        console.log(action);
        return Object.assign({}, state, {
            activeSelections: [
                ...state.activeSelections,
                action.payload.id,
            ],
        });

    case types.SELECTIONS_UNSELECT:
        return Object.assign({}, state, {
            activeSelections: state.activeSelections.filter(selection => selection !== action.payload.id),
        });

    default:
        return state;
    }
};

export default SelectionsReducer;
