import { combineReducers } from 'redux';

import DataReducer from './DataReducer';
import EventsReducer from './EventsReducer';
import MarketsReducer from './MarketsReducer';
import SelectionsReducer from './SelectionsReducer';

const reducers = combineReducers({
    DataReducer,
    EventsReducer,
    MarketsReducer,
    SelectionsReducer,
});

export default reducers;
