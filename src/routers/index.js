import React from 'react';
import {
    View,
    StyleSheet,
} from 'react-native';
import { DrawerNavigator } from 'react-navigation';

import EventScreen from './../containers/EventsScreen';

import Drawer from './../containers/Drawer';

const Root = DrawerNavigator({
    EventScreen: {
        screen: EventScreen,
    },
}, {
    initialRouteName: 'EventScreen',
    drawerWidth: 280,
    drawerPosition: 'right',
    contentComponent: Drawer,
});

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
    },
});

const RootRouter = () => (
    <View style={styles.wrapper}>
        <Root />
    </View>
);

export default RootRouter;
