import { call, put, takeLatest } from 'redux-saga/effects';

import organizeData from './../../helpers/DataHelper';

import * as actionTypes from './../../redux/action-types/DataActionTypes';
import { EVENTS_SET_DATA } from './../../redux/action-types/EventsActionTypes';
import { MARKETS_SET_DATA } from './../../redux/action-types/MarketsActionTypes';
import { SELECTIONS_SET_DATA } from './../../redux/action-types/SelectionsActionTypes';

import Api from './../../api';

function* loadData(action) {
    try {
        const data = yield call(Api.data.loadData);
        const organizedData = organizeData(data);

        yield put({ type: EVENTS_SET_DATA, events: organizedData.events });
        yield put({ type: MARKETS_SET_DATA, markets: organizedData.markets });
        yield put({ type: SELECTIONS_SET_DATA, selections: organizedData.selections });

        yield put({ type: actionTypes.CLEAR_ERROR_MESSAGE });
    } catch (error) {
        yield put({ type: actionTypes.LOAD_DATA_FAILED, message: error.message });
    }
}

function* loadDataSaga() {
    yield takeLatest(actionTypes.LOAD_DATA, loadData);
}

export default loadDataSaga;
