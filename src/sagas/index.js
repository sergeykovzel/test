import { fork } from 'redux-saga/effects';
import * as dataSagas from './data';

function* rootSaga() {
    yield [
        fork(dataSagas.loadDataSaga),
    ];
}

export default rootSaga;
